# wallstreetdocs test

tested on macos only.

##running the app

installing Postgres Server on mac
```
brew install postgresql
brew tap homebrew/services
brew services start postgresql
```
you can check to see if Postgres is running by entering:
```
lsof -i tcp:5432
```
Create a database
```
creatdb
> psql -h localhost
# CREATE DATABASE db;
# \q
```
Please edit src/database.js file and change 'username' to the current logged mac user

install packages
```
npm i
```

start app
```
node server.js
```
 
to test if files are saved to the database you can go to the url:
```
http://localhost:3000/api/file/info
```

to download saved files go to:
```
http://localhost:3000/api/file/:id
```
where :id is the file id

## running unit tests
to run unit tests run command:
```
npm t
```
