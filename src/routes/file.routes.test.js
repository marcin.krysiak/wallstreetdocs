var request = require('supertest');
var app = require('../../server');

describe('API /api/file', function() {
  describe('POST /api/file/upload', function() {
    it('should get success response when upload file', function (done) {
      request(app)
        .post('/api/file/upload')
        .attach('file', 'server.js')
        .expect(200)
        .then(function (res) {
          done();
        })
        .catch(function (err) {
          done(err)
        });
    });

    it('should response with error message when no file attached', function (done) {
      request(app)
        .post('/api/file/upload')
        .expect(400)
        .then(function (res) {
          done();
        })
        .catch(function (err) {
          done(err)
        });
    });
  });

  describe('GET /api/file/info', function() {
    it('should get list of uploaded files', function (done) {
      request(app)
        .get('/api/file/info')
        .expect(200)
        .then(function (res) {
          done();
        })
        .catch(function (err) {
          done(err)
        });
    });
  });

  describe('GET /api/file/:id', function() {
    it('should download file with id', function (done) {
      request(app)
        .get('/api/file/1')
        .expect(200)
        .then(function (res) {
          done();
        })
        .catch(function (err) {
          done(err)
        });
    });
  });
});